  <div class="swiper-container m_swiper2">
      <div class="swiper-wrapper">
          <div class="swiper-slide">
              <div class="s2_box">
                  <figure class="s2_img">
                      <img src="img/partner1.png">
                  </figure>
                  <figcaption class="m_s2_txt">
                      <h2>대표 <b>전용탁</b></h2>
                  </figcaption>
              </div>
              <div class="s2_box">
                  <figure class="s2_img">
                      <img src="img/partner2.png">
                  </figure>
                  <figcaption class="m_s2_txt">
                      <h2>부대표 <b>김은지</b></h2>
                  </figcaption>
              </div>
          </div>
          <div class="swiper-slide">
              <div class="s2_box">
                  <figure class="s2_img">
                      <img src="img/partner3.png">
                  </figure>
                  <figcaption class="m_s2_txt">
                      <h2><b>진아영</b></h2>
                  </figcaption>
              </div>
              <div class="s2_box">
                  <figure class="s2_img">
                      <img src="img/partner4.png">
                  </figure>
                  <figcaption class="m_s2_txt">
                      <h2><b>권민경</b></h2>
                  </figcaption>
              </div>
          </div>
      </div>
  </div>
  <img src="img/touch.gif" class="touch touchs">
